import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public show:boolean = true;
  public buttonName:any = 'Show';

  constructor() { }

  ngOnInit(): void {
  }

   myToggle() {
    this.show = !this.show;
  }

}
